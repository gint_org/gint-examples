# Description #

Gint Examples are example Gint scripts. Gint is short for **Gradle Integration Test and Automation Framework**.

# Links #

* [Gint Repository](https://bitbucket.org/gint_org/gint)
* [Gint Atlassian Repository](https://bitbucket.org/gint_org/gint-atlassian)
* [Gint Examples Repository](https://bitbucket.org/gint_org/gint-examples)
* [Documentation Site](https://ginthome.atlassian.net/wiki/display/GINT) - extensive user documentation
* [Jira Issues](https://ginthome.atlassian.net/browse/GINT) - support and improvement requests
* [Atlassian Command Line Interface (CLI)](https://bobswift.atlassian.net/wiki/display/ACLI)

# Getting Started #
1.  `git clone git@bitbucket.org:gint_org/gint-examples.git`
1.  `cd gint-examples`
1.  `./gradlew`

# Getting Started - Gint Atlassian#
1.  Install and configure Atlassian CLI for accessing your Cloud or Server instances
1.  Make sure you have a `acli` start script on your path and indicate by parameter that you want include running atlassian examples: `./gradlew -Datlassian`
1.  Configuration should include sites for jira, confluence, and jira-gint-test (Atlassian Cloud). Exclude examples that error due a missing configuration.
